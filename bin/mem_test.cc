#include "TTH/CommonClassifier/interface/MEMClassifier.h"
#include "TFile.h"
#include <iostream>

using namespace std;
using namespace MEM;

const TLorentzVector p4(double pt, double eta, double phi, double mass) {
    TLorentzVector lv;
    lv.SetPtEtaPhiM(pt, eta, phi, mass);
    return lv;
}

int main(){

  //Setup the MEM
  MEMClassifier mem;

  //Add some objects to the MEM
  std::vector<TLorentzVector> jets_p4_nominal = {
       p4(340.654388428, -2.27929639816, 1.07088756561, 28.3690547943),
       p4(169.823135376, -1.09558558464, -1.68332433701, 19.2100219727),
       p4(106.471191406, -1.73152184486, -2.89366841316, 14.1999740601),
       p4(91.0636901855, -1.16678476334, -2.92009735107, 11.6663913727),
       p4(58.3697357178, -1.38574934006, -0.656430959702, 8.05632019043),
       p4(31.2667369843, 0.307675540447, -0.434196174145, 5.55666637421),
       p4(30.9939250946, -0.466470509768, -2.50881528854, 6.00200891495)
  };
  
  auto leps_p4 = {
    p4(52.8751449585, -0.260020583868, -2.55171084404, 0.139569997787)
  };

  float jet_corr[jets_p4_nominal.size()] = {1.021,1.036,1.005,0.981,1.057,0.977,1.036};
  float jet_AbsoluteScaleUp[jets_p4_nominal.size()] = {1.046,1.044,1.038,1.049,1.061,1.013,0.979};
  float jet_AbsoluteScaleDown[jets_p4_nominal.size()] = {1.041,1.040,1.033,1.045,1.056,1.009,0.979};
  float jet_AbsoluteStatUp[jets_p4_nominal.size()] = {1.044,1.042,1.036,1.048,1.061,1.011,0.978};
  float jet_AbsoluteStatDown[jets_p4_nominal.size()] = {1.044,1.043,1.036,1.046,1.057,1.011,0.976};
  //vector<double> variations = {0.9, 1.0, 1.1};
  vector<vector<double>> jet_variations;
  for (unsigned int ijet=0; ijet<jets_p4_nominal.size(); ijet++) {
    vector<double> variations = {jet_AbsoluteScaleUp[ijet]/jet_corr[ijet],jet_AbsoluteScaleDown[ijet]/jet_corr[ijet],jet_AbsoluteStatUp[ijet]/jet_corr[ijet],jet_AbsoluteStatDown[ijet]/jet_corr[ijet]};
    jet_variations.push_back(variations);
  }

std::vector<std::vector<TLorentzVector> > jets_p4;
jets_p4.push_back(jets_p4_nominal);
for (unsigned int ijet=0; ijet<jets_p4_nominal.size(); ijet++) {
  std::vector<TLorentzVector> p4_var;
  //Here we have 4 factorized JES sources
  for (unsigned int i = 0; i<4; i++) {
    TLorentzVector lv;
    lv.SetPtEtaPhiM(jets_p4_nominal[ijet].Pt()*jet_variations[ijet][i],jets_p4_nominal[ijet].Eta(),jets_p4_nominal[ijet].Phi(),jets_p4_nominal[ijet].M()*jet_variations[ijet][i]);
    p4_var.push_back(lv);
  }
  jets_p4.push_back(p4_var);
}


std::vector<std::vector<double> > jets_csv;
std::vector<std::vector<MEMClassifier::JetType> > jets_type;
std::vector<std::vector<std::vector<double> > > jets_variations;
for (unsigned int i = 0; i<5; i++) {
jets_csv.push_back({
      0.0429882630706,
      0.990463197231,
      0.899696052074,
      0.979629218578,
      0.437245458364,
      0.996093869209,
      0.212953850627
    });
jets_type.push_back({
      MEMClassifier::JetType::RESOLVED,
      MEMClassifier::JetType::RESOLVED,
      MEMClassifier::JetType::RESOLVED,
      MEMClassifier::JetType::RESOLVED,
      MEMClassifier::JetType::RESOLVED,
      MEMClassifier::JetType::RESOLVED,
      MEMClassifier::JetType::RESOLVED,
    });
jets_variations.push_back(jet_variations);
}

//Assume that event category doesn't change
vector <bool> changes_jet_category;
for (int i=0; abs(i)<abs(jet_variations[0].size())+1; i++){
  changes_jet_category.push_back(false);
}

  //create a MET
  TLorentzVector lv_met;
  lv_met.SetPtEtaPhiM(92.1731872559,0., -1.08158898354, 0.);
  auto res = mem.GetOutput(
    leps_p4,
    {-1},
    jets_p4,
    jets_csv,
    jets_type,
    jets_variations,
    lv_met,
    changes_jet_category
  );

  std::cout << "mem=" << res[0].p << std::endl;
  for (unsigned int ivar=0; ivar < res[0].p_variated.size(); ivar++) {
    if (changes_jet_category[ivar+1] == false) {
      std::cout << "mem var " << ivar << " = " << res[0].p_variated.at(ivar) << std::endl;
    }
    else std::cout << "mem var " << ivar << " = " << res[ivar].p_variated.at(ivar) << std::endl;
  }
}
